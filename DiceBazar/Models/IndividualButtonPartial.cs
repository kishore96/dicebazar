﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace DiceBazar.Models
{
    public class IndividualButtonPartial
    {
        public string ButtonType { get; set; }
        public string Action { get; set; }
        public string Glyph { get; set; }
        public string Text { get; set; }

        public int? CategoryId { get; set; }
        public int? ProductId { get; set; }


        public string ActionParameter
        {
            get
            {
                var param = new StringBuilder(@"/");

                if (CategoryId != null && CategoryId > 0)
                {
                    param.Append(String.Format("{0}", CategoryId));
                }
                if (ProductId != null && ProductId > 0)
                {
                    param.Append(String.Format("{0}", ProductId));
                }

                return param.ToString();
            }
        }
    }
}