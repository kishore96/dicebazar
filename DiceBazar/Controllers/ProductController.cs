﻿using DiceBazar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Net;
using DiceBazar.ViewModel;

namespace DiceBazar.Controllers
{
    public class ProductController : Controller
    {
        private ApplicationDbContext db;
        public ProductController()
        {
            db = new ApplicationDbContext();
        }

        // GET: Product
        public ActionResult Index()
        {
            var products = db.Products.Include(p => p.Category);
            return View(products.ToList());
        }

        // GET: Book/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            var model = new ProductViewModel
            {
                Product = product,
                Categories = db.Categories.ToList()
            };
            return View(model);
        }

        // GET: Book/Create
        public ActionResult Create()
        {
            var categories = db.Categories.ToList();
            var model = new ProductViewModel
            {
                Categories = categories
            };
            return View(model);
        }

        // POST: Book/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductViewModel productViewModel)
        {
            var product = new Product
            {
                Name = productViewModel.Product.Name,
                Description = productViewModel.Product.Description,
                Category = productViewModel.Product.Category,
                CategoryId = productViewModel.Product.CategoryId,
                Price = productViewModel.Product.Price
            };

            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            productViewModel.Categories = db.Categories.ToList();
            return View(productViewModel);
        }

        // GET: Book/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }

            var model = new ProductViewModel
            {
                Product = product,
                Categories = db.Categories.ToList()
            };
            return View(model);
        }

        // POST: Book/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ProductViewModel productViewModel)
        {
            var product = new Product
            {
                Id = productViewModel.Product.Id,

                Name = productViewModel.Product.Name,
                Description = productViewModel.Product.Description,
                Category = productViewModel.Product.Category,
                CategoryId = productViewModel.Product.CategoryId,
                Price = productViewModel.Product.Price
            };

            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            productViewModel.Categories = db.Categories.ToList();
            return View(productViewModel);
        }

        // GET: Book/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            var model = new ProductViewModel
            {
                Product = product,
                Categories = db.Categories.ToList()
            };
            return View(model);
        }

        // POST: Book/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }
    }
}