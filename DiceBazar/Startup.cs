﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DiceBazar.Startup))]
namespace DiceBazar
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
